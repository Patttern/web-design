/**
 * Add meritorious firmware script
 *
 * @author Egor Babenko (e.babenko@lar.tech)
 */
"use strict";

$(function () {
  $('#transceiver').dropdown();

  $('#versionType').dropdown({
    onChange: function (value) {
      var major = $('#major div');
      var minor = $('#minor div');
      var release = $('#release div');
      switch (value) {
        case 'release':
          major.addClass('disabled').attr('disabled', 'disabled');
          minor.addClass('disabled').attr('disabled', 'disabled');
          release.removeClass('disabled').removeAttr('disabled');
          break;
        case 'minor':
          $('#release .or').attr('data-text', '01');
          major.addClass('disabled').attr('disabled', 'disabled');
          release.addClass('disabled').attr('disabled', 'disabled');
          minor.removeClass('disabled').removeAttr('disabled');
          break;
        case 'major':
          $('#minor .or').attr('data-text', '00');
          $('#release .or').attr('data-text', '01');
          minor.addClass('disabled').attr('disabled', 'disabled');
          release.addClass('disabled').attr('disabled', 'disabled');
          major.removeClass('disabled').removeAttr('disabled');
          break;
        default:
          major.addClass('disabled').attr('disabled', 'disabled');
          minor.addClass('disabled').attr('disabled', 'disabled');
          release.addClass('disabled').attr('disabled', 'disabled');
          break;
      }
    }
  });

  $('i.add,i.remove').popup();

  $('i.remove').on('click', function () {
    var tr = $(this).closest('tr');
    var title = tr.find('.title').text();
    var message = 'Вы действительно желаете удалить компонент %s?';
    if (confirm(sprintf(message, title))) {
      console.info('TODO: Remove component');
    }
  });

  var switchCount = function () {
    var parent = $(this).parent();
    var minVal = parent.attr('id') === 'release' ? 1 : 0;
    var value = parseInt(parent.find('.or').attr('data-text'));
    if ($(this).hasClass('up')) {
      value++;
    } else {
      value--;
    }
    if (value < minVal) {
      value = minVal;
    }
    parent.find('.or').attr('data-text', sprintf('%02d', value));
  };

  $('.down').on('click', switchCount);
  $('.up').on('click', switchCount);
});
