/**
 * Device script
 *
 * @author Egor Babenko (e.babenko@lar.tech)
 */
"use strict";

$(function () {
  $('.search.icon').on('click', function () {
    $('.filter-content').toggleClass('closed');
  });
  $('.dropdown').dropdown();

  $('#groupaction').dropdown({
    onChange: function (value) {
      switch (value) {
        case 'action1':
        case 'action2':
          $('.changetags').hide();
          $('.changeapps').show();
          $('.actionbtn').show();
          break;
        case 'action3':
        case 'action4':
          $('.changeapps').hide();
          $('.changetags').show();
          $('.actionbtn').show();
          break;
        case 'action5':
        case 'action6':
          $('.changeapps').hide();
          $('.changetags').hide();
          $('.actionbtn').show();
          break;
        default:
          $('.changeapps').hide();
          $('.changetags').hide();
          $('.actionbtn').hide();
          break;
      }
    }
  });

  $('.changeapps, .changetags, .actionbtn').hide();

  $('#linkapp').on('click', function () {
    if ($('#appselector').hasClass('closed')) {
      $('#appselector').removeClass('closed');
    }
    // $('#appselector').toggleClass('closed');
  });
});