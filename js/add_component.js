/**
 * Add component script
 *
 * @author Egor Babenko (e.babenko@lar.tech)
 */
"use strict";

$(function () {
  $('#manufacturer').dropdown();
  $('#remoteSource').dropdown();

  $('#binSource').dropdown({
    onChange: function (value) {
      switch (value) {
        case 'remote':
          $('.localSrc').hide();
          $('.afterload').hide();
          $('#localSource').val('');
          $('.remoteSrc').show();
          break;
        case 'local':
          $('.remoteSrc').hide();
          $('#remoteSource').dropdown('clear');
          $('.localSrc').show();
          break;
      }
    }
  });

  $('#componentType').dropdown({
    onChange: function (value) {
      if (value) {
        $('.localSrc').hide();
        $('.afterload').hide();
        $('#localSource').val('');
        $('.remoteSrc').hide();
        $('#remoteSource').dropdown('clear');
        $('#binSource').dropdown('clear');
        $($('#binSource')
          .parent().get(0))
          .removeClass('disabled')
          .removeAttr('disabled');
      }
    }
  });

  $('i.add,i.remove').popup();

  $('.remoteSrc, .localSrc, .afterload').hide();
  $($('#binSource').parent().get(0)).addClass('disabled');

  $("#divUpload, #localSource").on("click", function() {
    $('#uploadFile').click();
  });

  $('#uploadFile')
    .on('click', function () {
      $('.afterload').hide();
    })
    .on('change', function (ev) {
      $('#divUpload')
        .removeClass('loading')
        .addClass('loading')
        .addClass('disabled')
        .attr('disabled', 'disabled');
      setTimeout(function () {
        console.info('TODO: Submit file to server');
        $('#localSource').val(ev.target.files[0].name);
        $('#uploadFile').val('');
        $('#divUpload')
          .removeClass('disabled')
          .removeClass('loading')
          .addClass('upload')
          .removeAttr('disabled');
        $('.afterload').show();
      }, 1000);
    });
});
