/**
 * Meritorious script
 *
 * @author Egor Babenko (e.babenko@lar.tech)
 */
"use strict";

$(function () {
  $('.dropdown').dropdown();

  var initTree = function () {
    $('.node').hide();
    $('.header .icon, .header .title')
      .off('click')
      .on('click', function () {
        $(this).closest('.content').find('.node').first().toggle();
      });
  };

  initTree();
});
